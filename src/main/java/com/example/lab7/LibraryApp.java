package com.example.lab7;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class LibraryApp extends Application {
    private TextArea textArea = new TextArea();
    private TextArea threadInfoArea = new TextArea();
    private Thread[] readers;
    private TextField readerCountField = new TextField("10");
    private TextField readTimeField = new TextField("2000");

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Library Simulation");
        VBox vbox = new VBox(textArea, threadInfoArea);
        Scene scene = new Scene(vbox, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

        Button startAllButton = new Button("Start All");
        startAllButton.setOnAction(e -> startAllReaders());

        Button stopAllButton = new Button("Stop All");
        stopAllButton.setOnAction(e -> stopAllReaders());

        vbox.getChildren().addAll(new Label("Reader count:"), readerCountField, new Label("Read time (ms):"), readTimeField, startAllButton, stopAllButton);
    }

    private void updateThreadInfo(String message) {
        Platform.runLater(() -> {
            threadInfoArea.appendText(message + "\n");
        });
    }

    private void startAllReaders() {
        int readerCount = Integer.parseInt(readerCountField.getText());
        int readTime = Integer.parseInt(readTimeField.getText());

        Library library = new Library(100, textArea);
        readers = new Thread[readerCount];
        Random random = new Random();
        for (int i = 0; i < readers.length; i++) {
            readers[i] = new Thread(new Reader(library, threadInfoArea, readTime), "Reader " + (i + 1));
            readers[i].setPriority(random.nextInt(10) + 1);
            readers[i].start();
        }
    }

    private void stopAllReaders() {
        for (Thread reader : readers) {
            reader.interrupt();
            updateThreadInfo("Thread info: " + reader.getName() + ", State: " + reader.getState() + ", Priority: " + reader.getPriority() + ", Time: " + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
        }
    }
}
