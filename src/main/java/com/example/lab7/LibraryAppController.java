package com.example.lab7;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LibraryAppController {
    private List<Thread> runningThreads = new ArrayList<>();

    @FXML
    private TextField numberOfThreadsField;

    @FXML
    private Button startThreadsButton;

    @FXML
    private VBox threadsContainer;


    @FXML
    protected void onHelloButtonClick() {
        System.out.println("Clicked");
        startThreads();
    }

    @FXML
    protected void onStopButtonClick() {
        System.out.println("Clicked 2");
        stopThreads();
    }

    private void startThreads() {
        int numberOfThreads = Integer.parseInt(numberOfThreadsField.getText());
        System.out.println("Threads: " + numberOfThreads);
        
        System.out.println("New thread panel is created:");

        // Create and start threads
        for (int i = 1; i <= numberOfThreads; i++) {
            ThreadInfoPanel threadInfoPanel = new ThreadInfoPanel("Thread " + i);
            System.out.println("New thread panel is created:");
            Thread thread = createThread(threadInfoPanel);
            thread.start();
            runningThreads.add(thread);
        }
    }

    private Thread createThread(ThreadInfoPanel threadInfoPanel) {
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws Exception {
                Random random = new Random();
                for (int i = 0; i < 10; i++) {
                    updateMessage("Running");
                    updateProgress(i, 10);
                    Thread.sleep(random.nextInt(1000));
                }
                updateMessage("Finished");
                updateProgress(10, 10);
                return null;
            }
        };

        task.messageProperty().addListener((observable, oldValue, newValue) -> {
            threadInfoPanel.setStatusLabel(newValue);
            threadInfoPanel.setTimestampLabel(System.currentTimeMillis());
        });

        Thread thread = new Thread(task);
        thread.setPriority(Thread.MIN_PRIORITY + (int) (Math.random() * (Thread.MAX_PRIORITY - Thread.MIN_PRIORITY + 1)));
        threadInfoPanel.setThread(thread);
        threadsContainer.getChildren().add(threadInfoPanel);
        return thread;
    }

    private void stopThreads() {
        for (Thread thread : runningThreads) {
            thread.interrupt();
        }
        runningThreads.clear();
    }
}

class ThreadInfoPanel extends HBox {
    private final Label nameLabel;
    private final Label statusLabel;
    private final Label priorityLabel;
    private final Label timestampLabel;

    public ThreadInfoPanel(String threadName) {
        System.out.println("Thread is here:");
        nameLabel = new Label(threadName);
        statusLabel = new Label("Waiting");
        priorityLabel = new Label("Priority: ");
        timestampLabel = new Label("Timestamp: ");
        getChildren().addAll(nameLabel, statusLabel, priorityLabel, timestampLabel);
    }

    public void setStatusLabel(String status) {
        statusLabel.setText("Status: " + status);
    }

    public void setThread(Thread thread) {
        priorityLabel.setText("Priority: " + thread.getPriority());
    }

    public void setTimestampLabel(long timestamp) {
        timestampLabel.setText("Timestamp: " + timestamp);
    }
}