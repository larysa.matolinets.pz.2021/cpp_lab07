package com.example.lab7;
import javafx.scene.control.TextArea;
import java.util.concurrent.Semaphore;

class Library {
    private int totalBooks;
    private TextArea textArea;
    private Semaphore semaphore;

    public Library(int totalBooks, TextArea textArea) {
        this.totalBooks = totalBooks;
        this.semaphore = new Semaphore(totalBooks);
        this.textArea = textArea;
    }

    public void borrowBook() throws InterruptedException {
        semaphore.acquire();
        synchronized(this) {
            totalBooks--;
            updateUI(Thread.currentThread().getName() + " has taken a book. Total books left: " + totalBooks);
        }
    }

    public void returnBook() {
        synchronized(this) {
            totalBooks++;
            updateUI(Thread.currentThread().getName() + " has returned a book. Total books left: " + totalBooks);
        }
        semaphore.release(); // збільшує кількість доступних дозволів на одиницю
    }

    private void updateUI(String message) {
        javafx.application.Platform.runLater(() -> textArea.appendText(message + "\n"));
    }
}