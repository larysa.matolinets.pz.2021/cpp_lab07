package com.example.lab7;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Semaphore;

class Reader implements Runnable {
    private Library library;
    private TextArea textArea;
    private int readTime;

    public Reader(Library library, TextArea textArea, int readTime) {
        this.library = library;
        this.textArea = textArea;
        this.readTime = readTime;
    }

    @Override
    public void run() {
        try {
            while (true) {
                library.borrowBook();
                updateThreadInfo("Thread info: " + Thread.currentThread().getName() + ", State: " + Thread.currentThread().getState() + ", Priority: " + Thread.currentThread().getPriority() + ", Time: " + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
                Thread.sleep(readTime);
                library.returnBook();
            }
        } catch (InterruptedException e) {
            updateThreadInfo("Thread info: " + Thread.currentThread().getName() + ", State: " + Thread.currentThread().getState() + ", Priority: " + Thread.currentThread().getPriority() + ", Time: " + LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
            Thread.currentThread().interrupt(); //дозволяє потоку вийти з циклу, коли він переривається
        }
    }

    private void updateThreadInfo(String message) {
        Platform.runLater(() -> {
            textArea.appendText(message + "\n");
        });
    }
}
